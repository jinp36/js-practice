// Please consider the following Python function that tests if a
// dictionary has a particular key with a specified value:

// def has_key_with_value(dictionary, key, value):
//     if key not in dictionary:
//         return False
//     return dictionary[key] == value
// Recall that Python dictionaries and JavaScript objects act similarly.

// Write an equivalent JavaScript function named hasPropertyWithValue
// that determines if a JavaScript object has a property with the
// specified name that has the specified value.

function hasKeyWithValue(object, key, value) {
    
  }

console.log(hasKeyWithValue({"a": 1, "b": 2, "c": 3}, "a", 1))
