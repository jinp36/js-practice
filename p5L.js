// Please consider the following Python function, which takes a list of two-element
// lists and creates a dictionary from them:

// def dictionary_from_pairs(pairs):
//     d = {}
//     for pair in pairs:
//         key = pair[0]
//         value = pair[1]
//         d[key] = value
//     return d
// For example, given the input [["a", 1], ["b", 2]],
// it would create the dictionary {'a': 1, 'b': 2}.

// Write an equivalent JavaScript function named
// objectFromPairs that takes an array of two-element arrays
// and creates an object whose properties and values come
// from each two-element array in the input.

// For example, given the input [['a', 1], ['b', 2]],
// it would return an object of the form {a: 1, b: 2}.


