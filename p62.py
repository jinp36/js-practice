# // Write a function called "computeSumOfAllElements".

# // Given an array of numbers, "computeSumOfAllElements"
# // returns the sum of all the elements in the given array.
# // If input array is empty, your function should return 0.

# // let result1 = computeSumOfAllElements([1, 2, 3]);
# // console.log('should log 6:', result1);

# // let result2 = computeSumOfAllElements([]);
# // console.log('should log 0:', result2);

# function computeSumOfAllElements(numbers) {
#     return numbers.reduce((sum, n) => sum + n)

# }
#  do it in python

def compute_sum_of_all_elements(arr):
    pass

print(compute_sum_of_all_elements([1, 2, 3]))
