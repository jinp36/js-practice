// Please consider the following Python function that returns a list
// of strings that contain the given substring:

// def find_strings_with_substring(strings, substring):
//     results = []
//     for string in strings:
//         if substring in string:
//             results.append(string)
//     return results
// For example, given the list of strings ["aaa", "baa", "ccc"] and
// the substring "aa", the return value would be ["aaa", "baa"]
// since only those two strings contain the "aa" substring.
// Write an equivalent JavaScript function named findStringsWithSubstring.
// Look at the includes method  of the JavaScript String object
// for a potential way to solve this problem.

function find_strings_with_substring(strings, substring) {
    
}

console.log(find_strings_with_substring(["aaa", "baa", "ccc"], 'aa'))
